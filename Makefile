-include .env
export

COLOR=\033[0;32m
NC=\033[0m
BOLD=\033[1m

COMPOSE_CMD=docker-compose -f docker-compose.yml
COMPOSE_SERVICE=image_optim
COMPOSE_RUN_CMD=${COMPOSE_CMD} run --rm ${COMPOSE_SERVICE}

DOCKER_USER=$(shell echo $$USER)
DOCKER_USER_ID=$(shell id -u $$USER)

##
## ---------------------
## Available make target
## ---------------------
##

all: help

help:
	@grep -E '(^[a-zA-Z0-9_-.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init:
	@cp .env.example .env
	@echo "${COLOR}[INFOS]${NC} New '.env' file generated from default development values"
	@echo "${COLOR}[INFOS]${NC} Update 'IMG_DIR' variable, to mount your image directory inside the ${COMPOSE_SERVICE} Docker container"

build:
	@${COMPOSE_CMD} build --no-cache

install: init build ## Init .env files & build Docker images

ssh: ## Start new Shell terminal inside the "image_optim" container
	${COMPOSE_RUN_CMD} sh

optim.images: optim.png optim.jpeg ## Lauch the optimisation of the images

optim.png:
	-@${COMPOSE_RUN_CMD} sh -c 'find -type f -name "*.png" | xargs optipng -o7'

optim.jpeg:
	-@${COMPOSE_RUN_CMD} sh -c 'find -type f -name "*.jpg" | xargs jpegoptim --all-progressive --strip-all --force'
